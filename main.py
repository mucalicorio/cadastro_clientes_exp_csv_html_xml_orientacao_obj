from dataclasses import dataclass
from typing import List
from abc import ABC, abstractmethod

from datetime import datetime


@dataclass
class Cliente:
    nome: str
    email: str
    telefone: List[str]


class ExportadorCliente(ABC):
    @abstractmethod
    def exportar(self, clientes: List[Cliente]):
        pass


class ExportarXML(ExportadorCliente):
    now = datetime.now()
    nome_arquivo = 'clientes_' + str(now.year) + '-' + str(now.month) + '-' + \
        str(now.day) + '_' + str(now.hour) + '-' + \
        str(now.minute) + '-' + str(now.second) + '.xml'

    def exportar(self, clientes: List[Cliente]):
        arquivo = open(self.nome_arquivo, 'a')
        conteudo = '<clientes>\n'
        for cliente in clientes:
            conteudo += '<cliente>\n<nome>' + cliente.nome + '</nome>\n<email>' + cliente.email + \
                '</email>\n<telefone>' + \
                str(cliente.telefone) + '</telefone>\n</cliente>\n'

        conteudo += '</clientes>'
        # print(conteudo)
        arquivo.write(conteudo)
        return 'Retorno XML'


class ExportarCSV(ExportadorCliente):
    now = datetime.now()
    nome_arquivo = 'clientes_' + str(now.year) + '-' + str(now.month) + '-' + \
        str(now.day) + '_' + str(now.hour) + '-' + \
        str(now.minute) + '-' + str(now.second) + '.csv'

    def exportar(self, clientes: List[Cliente]):
        arquivo = open(self.nome_arquivo, 'a')
        for cliente in clientes:
            conteudo = cliente.nome + ';' + \
                cliente.email + ';' + str(cliente.telefone) + '\n'
            # print(conteudo)
            arquivo.write(conteudo)

        return 'Retorno CSV'


class ExportarHTML(ExportadorCliente):
    now = datetime.now()
    nome_arquivo = 'clientes_' + str(now.year) + '-' + str(now.month) + '-' + str(now.day) + \
        '_' + str(now.hour) + '-' + str(now.minute) + \
        '-' + str(now.second) + '.html'

    def exportar(self, clientes: List[Cliente]):
        arquivo = open(self.nome_arquivo, 'a')
        conteudo = '<html>\n<body>\n'
        for cliente in clientes:
            conteudo += '<div>\n<h2>Nome:</h2>\n<p> ' + cliente.nome + \
                '</p>\n<h2>Email:</h2>\n<p> ' + cliente.email + \
                '</p>\n<h2>Telefone:</h2>\n<p> ' + \
                str(cliente.telefone) + '</p>\n</div>'
        conteudo += '</body>\n</html>'
        # print(conteudo)
        arquivo.write(conteudo)

        return 'Retorno HTML'


class ExportarComposto(ExportadorCliente):
    def exportar(self, clientes: List[Cliente]):
        print(ExportarHTML().exportar(clientes))
        print(ExportarCSV().exportar(clientes))
        print(ExportarXML().exportar(clientes))


def valida_resposta(tipo_resposta='Cliente'):
    resposta = input(
        '\n\n' + tipo_resposta + ': Deseja continuar respondendo? 1 - Sim, 2 - Não\nResposta: ')

    if (resposta != '1') and (resposta != '2'):
        print('\nResposta Inválida')
        valida_resposta(tipo_resposta)

    else:
        return resposta


def main():
    clientes = [
        Cliente(nome="Flávio", email="fulano@gmail.com",
                telefone=["(14) 9 9746-1232"]),
        Cliente(nome="Samuel", email="samuel@gmail.com",
                telefone=["(14) 9 9746-4060", "(14) 3488-1303"]),
        Cliente(nome="José Elias", email="ze_elias@gmail.com",
                telefone=["(14) 9 9746-4232", "(14) 3488-1303", "(14) 3488-2213"]),
        Cliente(nome="Gabriel", email="gabriel@gmail.com",
                telefone=["(14) 9 9746-4242", "(14) 3248-1303", "(14) 3488-2130"])
    ]

    resposta = ''
    while resposta != '2':
        nome = input('\n\nNome: ')
        email = input('Email: ')

        resposta = ''
        telefones = []
        while resposta != '2':
            telefone = input('\nTelefone: ')
            telefones.append(telefone)

            resposta = valida_resposta('Telefone')

        clientes.append(Cliente(nome=nome, email=email, telefone=telefones))

        resposta = valida_resposta()

    exportador = ExportarComposto()
    exportador.exportar(clientes)


if __name__ == '__main__':
    main()
