

public class Main {

    interface Writer {
        void write(Object obj);
    }

    static class FileNamedWriter implements Writer {
        String name;

        public FileNamedWriter(String name) {
            this.name = name;
        }

        public void write(Object obj) {
            String output = String.format("%s: %s", this.name obj.toString());
            System.err.println(output);
        }
    }

    static class NamedWriter implements Writer {
        String name;

        public NamedWriter(String name) {
            this.name = name;
        }

        public void write(Object obj) {
            String output = String.format("%s: %s", this.name obj.toString());
            System.out.println(output);
        }
    }


    public static void main(String[] args) {
        Writer diego = new FileNamedWriter("Diego");
        Writer gabriel = new NamedWriter("Gabriel");

        System.out.println(diego);
        System.out.println(gabriel);

        diego.write("Palmeiras não tem mundial!");
        gabriel.write("Pra quê que serve isso?");
    }
}
